import React, { Component } from "react";
import CourseItem from "../../Components/CourseItems";
import { connect } from "react-redux";
import { fetchCourses } from "../../Redux/Actions/user";

class HomeScreen extends Component {
  renderCourses = () => {
    return this.props.courseList.map((item, index) => (
      <div className="col-3 mb-4" key={index}>
        <CourseItem item={item} index={index} />
      </div>
    ));
  };

  render() {
    return (
      <div>
        <h1 className="display-4 text-center">Danh Sách Khóa Học</h1>
        <div className="container">
          <div className="row">{this.renderCourses()}</div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    // axios return promise ES6
    this.props.dispatch(fetchCourses());
  }
}

const mapStateToProps = (state) => ({
  courseList: state.course.courses,
});

export default connect(mapStateToProps)(HomeScreen);
