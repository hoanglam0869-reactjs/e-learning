import React, { Component } from "react";
import { connect } from "react-redux";
import { courseService } from "../../Services";
import { fetchCourseDetailAction } from "../../Redux/Actions";

class CourseDetailScreen extends Component {
  render() {
    const { hinhAnh, tenKhoaHoc } = this.props.courseDetail;
    return (
      <div>
        <img
          src={hinhAnh}
          alt="course detail"
          onError={({ target }) => {
            target.onerror = null;
            target.src = "https://picsum.photos/200";
          }}
        />
        <h3>{tenKhoaHoc}</h3>
      </div>
    );
  }

  componentDidMount() {
    courseService
      .fetchCoursesDetail()
      .then((res) => {
        this.props.dispatch(fetchCourseDetailAction(res.data));
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

const mapStateToProps = (state) => ({
  courseDetail: state.course.courseDetail || {
    biDanh: "",
    danhMucKhoaHoc: { maDanhMucKhoahoc: "", tenDanhMucKhoaHoc: "" },
    hinhAnh: "https://picsum.photos/200",
    luotXem: 0,
    maKhoaHoc: "",
    maNhom: "",
    moTa: "",
    ngayTao: "",
    nguoiTao: {
      taiKhoan: "",
      hoTen: "",
      maLoaiNguoiDung: "",
      tenLoaiNguoiDung: "",
    },
    soLuongHocVien: 0,
    tenKhoaHoc: "",
  },
});

export default connect(mapStateToProps)(CourseDetailScreen);
