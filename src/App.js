import "./App.css";
import HomeScreen from "./Screens/Home";
import CourseDetailScreen from "./Screens/Detail";
import SignUpScreen from "./Screens/SignUp";
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" Component={HomeScreen} />
        <Route path="/detail" Component={CourseDetailScreen} />
        <Route path="/signup" Component={SignUpScreen} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
