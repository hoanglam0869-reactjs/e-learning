import { FETCH_COURSES, FETCH_COURSE_DETAIL } from "../Types";

// action creator
export const fetchCoursesAction = (payload) => ({
  type: FETCH_COURSES,
  payload,
});

export const fetchCourseDetailAction = (payload) => ({
  type: FETCH_COURSE_DETAIL,
  payload,
});
