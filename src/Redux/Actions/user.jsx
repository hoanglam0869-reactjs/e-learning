import { fetchCoursesAction } from ".";
import { courseService } from "../../Services";

// async action
export const fetchCourses = () => {
  return (dispatch) => {
    courseService
      .fetchCourses()
      .then((res) => {
        dispatch(fetchCoursesAction(res.data));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
