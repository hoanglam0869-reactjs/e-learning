import { FETCH_COURSES, FETCH_COURSE_DETAIL } from "../Types";

const initialState = {
  courses: [],
  courseDetail: null,
};

const CourseReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_COURSES: {
      state.courses = action.payload;
      return { ...state };
    }
    case FETCH_COURSE_DETAIL: {
      console.log(action.payload);
      state.courseDetail = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};

export default CourseReducer;
