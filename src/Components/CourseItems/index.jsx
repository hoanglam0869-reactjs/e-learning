import React, { Component } from "react";

export default class CourseItem extends Component {
  render() {
    const { tenKhoaHoc, hinhAnh } = this.props.item;
    const { hoTen } = this.props.item.nguoiTao;

    return (
      <div className="card p-2 h-100">
        <img
          src={hinhAnh}
          style={{ width: "100%", height: 200 }}
          onError={({ target }) => {
            target.onerror = null;
            target.src = `https://picsum.photos/200?random=${this.props.index}`;
          }}
        />
        <p className="lead fw-bold" style={{ minHeight: 90 }}>
          {tenKhoaHoc}
        </p>
        <p className="lead" style={{ minHeight: 30 }}>
          {hoTen}
        </p>
        <p className="lead">Rating: 5.0</p>
        <button className="btn btn-success">Go To Detail</button>
      </div>
    );
  }
}
