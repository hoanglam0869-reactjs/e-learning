import Axios from "axios";
import { configHeaders } from "./config";
import { BASE_URL } from "./constant";

class CourseService {
  fetchCourses = () => {
    return Axios({
      method: "GET",
      url: `${BASE_URL}/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01`,
      headers: configHeaders,
    });
  };
  fetchCoursesDetail = () => {
    return Axios({
      method: "GET",
      url: `${BASE_URL}/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=12345222`,
      headers: configHeaders,
    });
  };
}

export default CourseService;
