import Axios from "axios";
import * as yup from "yup";
import { configHeaders } from "./config";
import { BASE_URL } from "./constant";

export const signUpUserSchema = yup.object().shape({
  taiKhoan: yup.string().required("* Field is required!"),
  matKhau: yup.string().required("* Field is required!"),
  hoTen: yup.string().required("* Field is required!"),
  email: yup
    .string()
    .required("* Field is required!")
    .email("* Email is invalid!"),
  soDT: yup
    .string()
    .matches(/^[0-9]+$/g, "* SDT is invalid!")
    .required("* Field is required!"),
  maNhom: yup.string().required("* Field is required!"),
});

class UserService {
  signUp = (data) => {
    return Axios({
      method: "POST",
      url: `${BASE_URL}/api/QuanLyNguoiDung/DangKy`,
      data: data,
      headers: configHeaders,
    });
  };
}

export default UserService;
